﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Funcionario
    {
        [Display(Name ="Código")]
        public int Codigo { get; set; }
        public string Nome { get; set; }
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }
        [Display(Name = "Gênero")]
        public string Genero { get; set; }
    }
}