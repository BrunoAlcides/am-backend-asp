﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Sobre
    {
        [Display(Name = "Código")]
        [Key]
        public int Codigo { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Descricao { get; set; }
    }
}