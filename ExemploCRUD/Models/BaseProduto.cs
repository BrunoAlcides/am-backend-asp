﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class BaseProduto
    {
        public Produto Produto { get; set; }
        public Categoria Categoria { get; set; }
        public Platafoma Platafoma { get; set; }
        public Usuario Usuario { get; set; }

        public List<Produto> Produtos { get; set; }
        public List<Categoria> Categorias { get; set; }
        public List<Platafoma> Platafomas { get; set; }
        public List<Usuario> Usuarios { get; set; }
    }
}