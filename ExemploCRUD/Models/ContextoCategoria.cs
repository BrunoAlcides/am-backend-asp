﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExemploCRUD.Models
{
    public class ContextoCategoria : DbContext
    {
        public ContextoCategoria()
        {
        }
        public DbSet<Categoria> Categorias { get; set; }
    }
}