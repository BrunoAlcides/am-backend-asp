﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExemploCRUD.Models
{
    public class ContextoSobre : DbContext
    {
        public ContextoSobre()
        {
        }
        public DbSet<Sobre> Sobres { get; set; }
    }
}