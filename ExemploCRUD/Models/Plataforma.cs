﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Platafoma
    {
        [Display(Name = "Código")]
        [Key]
        public int Codigo { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Titulo { get; set; }
    }
}