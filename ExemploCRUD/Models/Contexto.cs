﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExemploCRUD.Models
{
    public class Contexto:DbContext
    {
        public Contexto() {
        }
        public DbSet<Usuario> Usuarios { get; set; }
    }
}