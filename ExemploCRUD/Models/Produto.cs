﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Produto
    {
        public string Imagem { get; set; }

        [Display(Name = "Código")]
        [Key]
        public int Codigo { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Titulo { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Campo obrigatório!")]
        public int Categoria { get; set; }

        [Required(ErrorMessage = "Campo obrigatório!")]
        public int Plataforma { get; set; }

        public int Usuario { get; set; }
        public int Usuario_vencedor { get; set; }
        public double Lance { get; set; }
    }
}