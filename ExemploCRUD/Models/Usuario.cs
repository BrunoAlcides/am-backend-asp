﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ExemploCRUD.Models
{
    public class Usuario
    {
        [Display(Name = "Código")]
        [Key]
        public int Codigo { get; set; }
        [Required(ErrorMessage ="Campo obrigatório!")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Campo obrigatório!")]
        public string Senha { get; set; }
        public int Tipo { get; set; }

        public String ErroMensagemLogin { get; set; }
    }
}