﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExemploCRUD.Models
{
    public class ContextoProduto : DbContext
    {
        public ContextoProduto()
        {
        }
        public DbSet<Produto> Produtos { get; set; }
    }
}