﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ExemploCRUD.Models
{
    public class ContextoPlataforma : DbContext
    {
        public ContextoPlataforma()
        {
        }
        public DbSet<Platafoma> Plataformas { get; set; }
    }
}