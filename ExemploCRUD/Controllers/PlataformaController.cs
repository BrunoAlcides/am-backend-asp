﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExemploCRUD.Models;

namespace ExemploCRUD.Controllers
{
    public class PlataformaController : Controller
    {
        private ContextoPlataforma db = new ContextoPlataforma();

        // GET: Plataforma
        public ActionResult Index()
        {
            return View(db.Plataformas.ToList());
        }

        // GET: Plataforma/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platafoma platafoma = db.Plataformas.Find(id);
            if (platafoma == null)
            {
                return HttpNotFound();
            }
            return View(platafoma);
        }

        // GET: Plataforma/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Plataforma/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Titulo")] Platafoma platafoma)
        {
            if (ModelState.IsValid)
            {
                db.Plataformas.Add(platafoma);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(platafoma);
        }

        // GET: Plataforma/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platafoma platafoma = db.Plataformas.Find(id);
            if (platafoma == null)
            {
                return HttpNotFound();
            }
            return View(platafoma);
        }

        // POST: Plataforma/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Titulo")] Platafoma platafoma)
        {
            if (ModelState.IsValid)
            {
                db.Entry(platafoma).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(platafoma);
        }

        // GET: Plataforma/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Platafoma platafoma = db.Plataformas.Find(id);
            if (platafoma == null)
            {
                return HttpNotFound();
            }
            return View(platafoma);
        }

        // POST: Plataforma/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Platafoma platafoma = db.Plataformas.Find(id);
            db.Plataformas.Remove(platafoma);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
