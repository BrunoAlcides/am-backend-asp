﻿using ExemploCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExemploCRUD.Controllers
{
    [RoutePrefix("api/categorias")]
    public class CategotiaAPIController : ApiController
    {
        private ContextoCategoria db = new ContextoCategoria();

        [Route("")]
        public IEnumerable<Categoria> Get()
        {
            return db.Categorias.ToList();
        }

        [Route("{id}")]
        public Object Get(int id)
        {
            Categoria categoria = db.Categorias.Find(id);

            if (categoria == null)
            {
                return null;
            }

            return categoria;
        }
    }
}
