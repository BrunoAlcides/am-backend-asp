﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExemploCRUD.Models;

namespace ExemploCRUD.Controllers
{
    public class SobreController : Controller
    {
        private ContextoSobre db = new ContextoSobre();

        // GET: Sobre
        public ActionResult Index()
        {
            return View(db.Sobres.ToList());
        }

        // GET: Sobre/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sobre sobre = db.Sobres.Find(id);
            if (sobre == null)
            {
                return HttpNotFound();
            }
            return View(sobre);
        }

        // GET: Sobre/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sobre/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Descricao")] Sobre sobre)
        {
            if (ModelState.IsValid)
            {
                db.Sobres.Add(sobre);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sobre);
        }

        // GET: Sobre/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sobre sobre = db.Sobres.Find(id);
            if (sobre == null)
            {
                return HttpNotFound();
            }
            return View(sobre);
        }

        // POST: Sobre/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Descricao")] Sobre sobre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sobre).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sobre);
        }

        // GET: Sobre/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sobre sobre = db.Sobres.Find(id);
            if (sobre == null)
            {
                return HttpNotFound();
            }
            return View(sobre);
        }

        // POST: Sobre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sobre sobre = db.Sobres.Find(id);
            db.Sobres.Remove(sobre);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
