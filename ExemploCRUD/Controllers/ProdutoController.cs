﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExemploCRUD.Models;

namespace ExemploCRUD.Controllers
{
    public class ProdutoController : Controller
    {
        private ContextoProduto db = new ContextoProduto();

        // GET: Produto
        public ActionResult Index()
        {
            return View(db.Produtos.ToList());
        }

        public ActionResult IndexLeiloes()
        {
            return View(db.Produtos.ToList());
        }

        public ActionResult IndexLeiloesVencidos()
        {
            return View(db.Produtos.ToList());
        }

        // GET: Produto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Produto/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Codigo,Imagem,Titulo,Descricao,Categoria,Plataforma,Usuario,Usuario_vencedor,Lance")] Produto produto, HttpPostedFileBase Foto)
        {
            //var path = "";
            //if (Foto != null)
            //{
                //if (Foto.ContentLength > 0)
                //{
                    //if (System.IO.Path.GetExtension(Foto.FileName).ToLower() == ".jpg" || System.IO.Path.GetExtension(Foto.FileName).ToLower() == ".png" || System.IO.Path.GetExtension(Foto.FileName).ToLower() == ".gif" || System.IO.Path.GetExtension(Foto.FileName).ToLower() == ".jpeg")
                    //{
                        /*path = System.IO.Path.Combine(Server.MapPath("~/Content/Imagens"), Foto.FileName);
                        Foto.SaveAs(path);

                        produto.Imagem = Foto.FileName;*/

                        if (ModelState.IsValid)
                        {
                            produto.Usuario = Convert.ToInt32(Session["usuarioCodigo"]);
                            db.Produtos.Add(produto);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }

                    //}
                    //else
                    //{
                    //    ViewBag.ErrorMessage = "Selecine uma imagem válida!";
                    //}
                //}
            //}
            //else {
            //  ViewBag.ErrorMessage = "Erro! Selecione a imagem do produto!";
            //}

            return View(produto);
        }

        // GET: Produto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // GET: Produto/Edit/5
        public ActionResult EditLeiloes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // POST: Produto/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Codigo,Imagem,Titulo,Descricao,Categoria,Plataforma,Usuario,Usuario_vencedor,Lance")] Produto produto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(produto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLeiloes([Bind(Include = "Codigo,Imagem,Titulo,Descricao,Categoria,Plataforma,Usuario,Usuario_vencedor,Lance")] Produto produto)
        {
            double lanceAntigo = Convert.ToDouble(Session["maiorLance"].ToString());
            double lanceAtual = produto.Lance;
            if (ModelState.IsValid)
            {
                if (lanceAtual > lanceAntigo)
                {
                    produto.Usuario_vencedor = Convert.ToInt32(Session["usuarioCodigo"]);
                    db.Entry(produto).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("IndexLeiloes");
                }
                else {
                    produto.Lance = lanceAntigo;
                    ViewBag.ErrorMessage = "Atenção! O lance fornecido deve ser maior que o atual!";
                }
            }
            return View(produto);
        }

        // GET: Produto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // POST: Produto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produto produto = db.Produtos.Find(id);
            db.Produtos.Remove(produto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
