﻿using ExemploCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExemploCRUD.Controllers
{
    [RoutePrefix("api/plataformas")]
    public class PlataformaAPIController : ApiController
    {
        private ContextoPlataforma db = new ContextoPlataforma();

        [Route("")]
        public IEnumerable<Platafoma> Get()
        {
            return db.Plataformas.ToList();
        }

        [Route("{id}")]
        public Object Get(int id)
        {
            Platafoma plataforma = db.Plataformas.Find(id);

            if (plataforma == null)
            {
                return null;
            }

            return plataforma;
        }
    }
}
