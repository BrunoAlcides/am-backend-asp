﻿using ExemploCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExemploCRUD.Controllers
{
    public class HomeController : Controller
    {
        private ContextoSobre db = new ContextoSobre();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sobre()
        {
            using (ContextoSobre c = new ContextoSobre())
            {
                var sobreD = db.Sobres.Where(x => x.Codigo == 1).FirstOrDefault();
                Session["sobreCodigo"] = sobreD.Codigo;
                Session["sobreDescricao"] = sobreD.Descricao;
            }
            return View();
        }

        public ActionResult Patrocinio()
        {
            return View();
        }
    }
}