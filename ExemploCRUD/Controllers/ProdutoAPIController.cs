﻿using ExemploCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExemploCRUD.Controllers
{
    [RoutePrefix("api/leiloes")]
    public class ProdutoAPIController : ApiController
    {
        private Contexto db = new Contexto();
        private ContextoProduto dbProduto = new ContextoProduto();
        private ContextoCategoria dbCategoria = new ContextoCategoria();
        private ContextoPlataforma dbPlataforma = new ContextoPlataforma();

        [Route("")]
        public IEnumerable<Object> Get()
        {
            return dbProduto.Produtos.ToList().Select(preencherDependencias);
        }

        [Route("{id}")]
        public Object Get(int id)
        {
            Produto produto = dbProduto.Produtos.Find(id);

            if (produto == null)
            {
                return null;
            }

            return preencherDependencias(produto);
        }

        public Object preencherDependencias(Produto produto)
        {
            return new
            {
                Image = produto.Imagem,
                Codigo = produto.Codigo,
                Titulo = produto.Titulo,
                Descricao = produto.Descricao,
                Lance = produto.Lance,
                Categoria = produto.Categoria,
                CategoriaModel = dbCategoria.Categorias.Where(x => x.Codigo == produto.Categoria).FirstOrDefault(),
                Plataforma = produto.Plataforma,
                PlataformaModel = dbPlataforma.Plataformas.Where(x => x.Codigo == produto.Plataforma).FirstOrDefault(),
                Usuario = produto.Usuario,
                UsuarioModel = db.Usuarios.Where(x => x.Codigo == produto.Usuario).FirstOrDefault(),
                UsuarioVencedor = produto.Usuario_vencedor,
                UsuarioVencedorModel = db.Usuarios.Where(x => x.Codigo == produto.Usuario_vencedor).FirstOrDefault()
             };
        }

        [Route("{id}/novolance")]
        public Object Put(int id, int usuarioVencedorId, double novoLance)
        {
            Produto produto = dbProduto.Produtos.Find(id);

            if (produto == null)
            {
                return BadRequest("Lellão não encontrado");
            }

            if (novoLance < produto.Lance)
            {
                return BadRequest("Valor do novo lance não pode ser menor do que o lance atual");
            }

            produto.Lance = novoLance;
            produto.Usuario_vencedor = usuarioVencedorId;

            dbProduto.SaveChanges();

            return Ok(produto);
        }

        [Route("{id}")]
        public Object Put(int id, [FromBody]Produto novoProduto)
        {
            Produto produto = dbProduto.Produtos.Find(id);

            if (produto == null)
            {
                return BadRequest("Lellão não encontrado");
            }

            produto.Titulo = novoProduto.Titulo;
            produto.Descricao = novoProduto.Descricao;
            produto.Categoria = novoProduto.Categoria;
            produto.Plataforma = novoProduto.Plataforma;

            dbProduto.SaveChanges();

            return Ok(produto);
        }
    }
}
