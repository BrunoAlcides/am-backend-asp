﻿using ExemploCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExemploCRUD.Controllers
{
    [RoutePrefix("api/usuarios")]
    public class UsuarioAPIController : ApiController
    {
        private Contexto db = new Contexto();

        // GET: api/UsuarioAPI
        [Route("")]
        public IEnumerable<Usuario> Get()
        {
            return db.Usuarios.ToList();
        }

        // GET: api/UsuarioAPI/5
        [Route("{id}")]
        public Usuario Get(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);

            if (usuario == null)
            {
                return null;
            }

            return usuario;
        }

        [Route("login")]
        public Usuario Put([FromBody]Usuario usuario)
        {
            Usuario usuarioLogado = db.Usuarios.Where(x => x.Email == usuario.Email && x.Senha == usuario.Senha).FirstOrDefault();
            if (usuarioLogado == null)
            {
                Usuario error = new Usuario
                {
                    ErroMensagemLogin = "Usuário ou senha incorretos!"
                };

                return error;
            }

            return usuarioLogado;
        }

        [Route("")]
        public Usuario Post([FromBody]Usuario usuario)
        {
            if (usuario.Nome == "" || usuario.Nome == null || usuario.Email == "" || usuario.Email == null || usuario.Senha == "" || usuario.Senha == null)
            {
                return new Usuario
                {
                    ErroMensagemLogin = "Campos vazio"
                };
            }

            db.Usuarios.Add(usuario);
            db.SaveChanges();

            Usuario usuarioLogado = db.Usuarios.Where(x => x.Email == usuario.Email && x.Senha == usuario.Senha).FirstOrDefault();

            return usuarioLogado;
        }
    }
}
